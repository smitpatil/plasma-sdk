/*
    SPDX-FileCopyrightText: 2014-2015 Sebastian Kügler <sebas@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "iconmodel.h"

#include <QDebug>
#include <QIcon>
#include <QImageReader>

#include <QFuture>
#include <QtConcurrent/QtConcurrent>

#include <KIconLoader>
#include <KIconTheme>

#include <map>

using namespace CuttleFish;

static QTextStream cout(stdout);

IconModel::IconModel(QObject *parent) :
    QAbstractListModel(parent)
{
    m_roleNames.insert(FileName, "fileName");
    m_roleNames.insert(IconName, "iconName");
    m_roleNames.insert(Icon, "icon");
    m_roleNames.insert(FullPath, "fullPath");
    m_roleNames.insert(Category, "category");
    m_roleNames.insert(Scalable, "scalable");
    m_roleNames.insert(SizeFullPaths, "sizeFullPaths");
    m_roleNames.insert(Theme, "iconTheme");
    m_roleNames.insert(Type, "type");

    m_categories = QStringList() << "all" \
        << "actions"
        << "animations"
        << "apps"
        << "categories"
        << "devices"
        << "emblems"
        << "emotes"
        << "international"
        << "mimetypes"
        << "places"
        << "status";

    load(KIconLoader::global()->theme()->internalName());
}


QHash<int, QByteArray> IconModel::roleNames() const
{
    return m_roleNames;
}

int IconModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    const auto &icons = m_data[m_currentTheme].icons;
    if (icons.size() <= 0) {
        return 0;
    } else {
        return icons.size();
    }
}

QVariant IconModel::data(const QModelIndex &index, int role) const
{
    if (index.isValid()) {

        const auto &icon = m_data[m_currentTheme].icons[index.row()];
        const auto &data= m_data[m_currentTheme].data[icon];

        switch (role) {
        case IconName:
        case Icon:
            return icon;
        case FileName:
            return data.fileName;
        case FullPath:
            return data.bestFullPath;
        case Category:
            return data.category;
        case Scalable :
            return data.scalable;
        case Type:
            return data.type;
        case Theme:
            return m_currentTheme;
        }
    }
    return QVariant();
}

void IconModel::add(const QFileInfo &info, const QString &cat, const QString &iconTheme, int size)
{
    QString icon = info.fileName();
    bool scalable = false;
    if (icon.endsWith(QLatin1String(".png"))) {
        icon.remove(".png");
    } else if (icon.endsWith(QLatin1String(".svgz"))) {
        icon.remove(".svgz");
        scalable = true;
    } else if (icon.endsWith(QLatin1String(".svg"))) {
        icon.remove(".svg");
        scalable = true;
    }

    auto &data = m_data[iconTheme].data[icon];
    auto &icons = m_data[iconTheme].icons;

    //get size if KIconTheme::querySizes does not work
    if(size == -1) {
        QString sizeStr;
        for (QChar ch : info.absoluteFilePath()) {
            if(ch.isDigit())
                sizeStr.append(ch);
            else if(!sizeStr.isEmpty())
                break;
        }
        if(sizeStr.isEmpty()){
            QImageReader img(info.absoluteFilePath());
            if(img.canRead()){
                size = img.size().width();
            }
            else {
                size = -1;
            }
        }
        else {
            size = sizeStr.toInt();
        }
    }

    //store icon in data only if icon is new or icon is larger and not scalable
    if (!icons.contains(icon)  || (icons.contains(icon)  && data.size < size && !data.scalable ) ) {

        data.bestFullPath = info.absoluteFilePath();

        data.fileName = info.fileName();
        data.category = cat;
        data.type = QStringLiteral("icon");
        data.scalable = scalable;

        data.size = size;

        if(!icons.contains(icon))
            icons << icon;
    }

    //store size and full path
    auto &sizeFullPaths = data.sizeFullPaths;
    if(!sizeFullPaths.contains(size)) {
        sizeFullPaths[size] = info.absoluteFilePath();
    }

    if (scalable && !data.scalable) {
        data.scalable = true;
    }
}

QStringList IconModel::categories() const
{
    return m_categories;
}

QString IconModel::currentTheme() const
{
    return m_currentTheme;
}

QString IconModel::bestIcon(const QString &iconName, const QString &iconTheme, int size)
{
    const auto &data = m_data[iconTheme].data[iconName];
    const auto &iconSizes =  data.sizeFullPaths;

    const auto findex = iconSizes.find(size);
    if(findex != iconSizes.end())
        return findex.value();

    if(iconSizes.isEmpty()) {
        const auto fullPath = data.bestFullPath;
        if(fullPath.isEmpty())
            return iconName;
        return fullPath;
    }

    const auto match = iconSizes.upperBound(size);
    if(match == iconSizes.end())
        return iconSizes.last();
    return match.value();
}

void IconModel::load(const QString &iconTheme)
{
    //qDebug() << "\n -- Loading (category / filter) : " << m_category << m_filter;

    //sm_categories.clear();
    if (!KIconLoader::global()) {
        return;
    }

    static const std::map<KIconLoader::Context,QString> contexes = {
        {KIconLoader::Action, "actions"},
        {KIconLoader::Application, "apps"},
        {KIconLoader::Device, "devices"},
        {KIconLoader::MimeType, "mimetypes"},
        {KIconLoader::Animation, "animations"},
        {KIconLoader::Category, "categories"},
        {KIconLoader::Emblem, "emblems"},
        {KIconLoader::Emote, "emote"},
        {KIconLoader::International, "international" },
        {KIconLoader::Place, "places"},
        {KIconLoader::StatusIcon, "status"}
    };

    static int activeLoads = 0;

    if(m_data.contains(iconTheme)){
        m_currentTheme = iconTheme;
        endResetModel();
        emit currentThemeChanged(m_currentTheme);
        return;
    }

    beginResetModel();

    m_data.insert(iconTheme,IconModel::ThemeData{});

    m_loading = true;
    emit loadingChanged();


    auto future = QtConcurrent::run([iconTheme,this](){
        activeLoads++;

        KIconTheme theme(iconTheme);
        const auto& sizes = theme.querySizes(KIconLoader::Desktop);

        if(sizes.isEmpty()) {
            for(const auto &context : contexes ){
                for(const auto &icon : theme.queryIconsByContext(0,context.first) ){
                    add(icon,context.second,iconTheme,-1);
                }
            }
        }
        else {
            for(const auto &size: sizes){
                for(const auto &context : contexes ) {
                    for(const auto &icon : theme.queryIcons(size,context.first) ){
                        add(icon,context.second,iconTheme,size);
                    }
                }
            }
        }
    });

    auto watcher = new QFutureWatcher<void>;
    watcher->setFuture(future);
    connect(watcher,&QFutureWatcher<void>::finished,[iconTheme,watcher,this](){
        m_currentTheme = iconTheme;
        endResetModel();
        activeLoads--;
        m_loading = activeLoads;
        emit loadingChanged();
        emit currentThemeChanged(m_currentTheme);
        delete watcher;
    });

}

bool IconModel::loading()
{
    return m_loading;
}

void IconModel::output(const QString& text)
{
    cout << text.toLocal8Bit();
    cout.flush();
}

QVariantList IconModel::inOtherThemes(const QString& name, int iconSize)
{
    QVariantList list;
    const QStringList themes = KIconTheme::list();
    for (const auto& themeName : themes) {
        const KIconTheme theme(themeName);
        const QString iconPath = theme.iconPathByName(name, iconSize, KIconLoader::MatchBest);
        list.append(QVariantMap({{"themeName", themeName}, {"iconPath", iconPath}}));
    }
    return list;
}
