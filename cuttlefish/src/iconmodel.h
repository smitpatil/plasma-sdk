/*
    SPDX-FileCopyrightText: 2014-2015 Sebastian Kügler <sebas@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#ifndef CUTTLEFISHICONMODEL_H
#define CUTTLEFISHICONMODEL_H

#include <QAbstractListModel>
#include <QFileInfo>
#include <QFutureWatcher>

namespace CuttleFish {


class IconModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(QStringList categories READ categories NOTIFY categoriesChanged)
    Q_PROPERTY(bool loading READ loading NOTIFY loadingChanged);
    Q_PROPERTY(QString currentTheme READ currentTheme NOTIFY currentThemeChanged)

public:
    enum Roles {
        FileName = Qt::UserRole + 1,
        IconName,
        Icon,
        FullPath,
        Category,
        Scalable,
        SizeFullPaths,
        Type,
        Theme
    };

    explicit IconModel(QObject *parent = nullptr);

    Q_INVOKABLE QVariantList inOtherThemes(const QString& iconName, int size);

    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;

    void add(const QFileInfo &info, const QString &cat, const QString &iconTheme, int size);

    QStringList categories() const;
    QString currentTheme() const;

    bool loading();

    //returns nearest greater icon
    Q_INVOKABLE QString bestIcon(const QString &iconName, const QString &iconTheme, int size);

    Q_INVOKABLE void load(const QString &iconTheme);

    Q_INVOKABLE void output(const QString &text);


Q_SIGNALS:
    void categoriesChanged();
    void loadingChanged();
    void currentThemeChanged(QString theme);

private:
    QHash<int, QByteArray> m_roleNames;

    QString m_currentTheme;

    struct IconData {
        QString fileName;
        QString bestFullPath;
        bool scalable;
        QString type;
        QString category;
        int size;
        QMap <int,QString> sizeFullPaths;
    };

    struct ThemeData {
        QStringList icons; //for faster access
        QHash<QString,IconData> data; //icon and its data
    };

    QHash<QString, ThemeData> m_data;
    QStringList m_categories;
    QHash<QString, QString> m_categoryTranslations;

    bool m_loading;
};

} // namespace

#endif // CUTTLEFISHICONMODEL_H
